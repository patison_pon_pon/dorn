-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 30 2019 г., 14:13
-- Версия сервера: 10.1.37-MariaDB-0+deb9u1
-- Версия PHP: 7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dorn`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_t` varchar(255) DEFAULT NULL,
  `meta_d` varchar(255) DEFAULT NULL,
  `meta_k` varchar(255) DEFAULT NULL,
  `description` text,
  `image` varchar(255) DEFAULT NULL,
  `icon` varchar(255) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`, `meta_t`, `meta_d`, `meta_k`, `description`, `image`, `icon`, `created_at`, `updated_at`) VALUES
(2, 'Коттеджи и жилые дома', '', '', '', '<p>Категория 1</p>', '15538955395c9e90739b3cb.jpg', '15539355365c9f2cb0e2e11.png', 1551188142, 1553935537),
(3, 'Общественные здания, офисы', '', '', '', '', '15538955705c9e9092248d3.jpg', '15539355485c9f2cbca1c82.png', 1552844230, 1553935548),
(4, 'Дизайн - проектирование интерьеров', '', '', '', '', '15538956205c9e90c41c53e.jpg', '15539355615c9f2cc9dd6cd.png', 1552844286, 1553935562),
(5, 'Клубы и развлекательные обьекты', '', '', '', '', '15538957195c9e91271faa8.jpg', '15539355775c9f2cd990d69.png', 1552844346, 1553935577),
(6, 'Торговые павильоны', '', '', '', '', '15539352055c9f2b651dc68.jpg', '15539352055c9f2b65f15af.png', 1553935206, 1553935206),
(7, 'Благоустройство территорий и зданий', '', '', '', '', '15539352835c9f2bb3e6678.jpg', '15539352845c9f2bb4df2a6.png', 1553935285, 1553935285),
(8, 'Реконструкция обьектов', '', '', '', '', '15539353565c9f2bfce8cc1.jpg', '15539353575c9f2bfdeb4fe.png', 1553935358, 1553935358),
(9, '3D моделирование, подготовка предпроектных предложений, эскизные проекты', '', '', '', '', '15539354635c9f2c67a9398.jpg', '15539354655c9f2c6967479.png', 1553935465, 1553935465);

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1551183800),
('m130524_201442_init', 1551183803);

-- --------------------------------------------------------

--
-- Структура таблицы `object`
--

CREATE TABLE `object` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_t` varchar(255) DEFAULT NULL,
  `meta_d` varchar(255) DEFAULT NULL,
  `meta_k` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `object`
--

INSERT INTO `object` (`id`, `category_id`, `name`, `meta_t`, `meta_d`, `meta_k`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 'Мега-крутой коттедж по неизвестному адресу', '', '', '', '<h1>Заголовок</h1><p>Описание\r\n</p>', 1551187756, 1553944806);

-- --------------------------------------------------------

--
-- Структура таблицы `object_image`
--

CREATE TABLE `object_image` (
  `id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `sort` int(11) NOT NULL DEFAULT '100',
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `object_image`
--

INSERT INTO `object_image` (`id`, `object_id`, `image`, `title`, `description`, `sort`, `created_at`) VALUES
(22, 1, '15513477225c77b00a53cca.jpg', NULL, NULL, 100, 1551347727),
(26, 1, '15513477795c77b04338e0e.jpg', NULL, NULL, 100, 1551347787),
(27, 1, '15513477805c77b04418127.jpg', NULL, NULL, 100, 1551347787),
(28, 1, '15513477815c77b04541e8e.jpg', NULL, NULL, 100, 1551347787);

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_t` varchar(255) DEFAULT NULL,
  `meta_d` varchar(255) DEFAULT NULL,
  `meta_k` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`id`, `name`, `meta_t`, `meta_d`, `meta_k`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Проектирование', 'Проектирование', '', '', '<h1>Выполняем работы по проектированию<br></h1><p>что-то тут написал</p>', 1553941232, 1553947807),
(2, 'Наши работы', 'Наши работы', '', '', '<h1>Наши работы</h1>Наши работы описание', 1553944640, 1553944640),
(3, 'Студия', 'Связаться с нами', '', '', '<h1>Связаться с нами</h1>', 1553946656, 1553947210);

-- --------------------------------------------------------

--
-- Структура таблицы `project`
--

CREATE TABLE `project` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `meta_t` varchar(255) DEFAULT NULL,
  `meta_d` varchar(255) DEFAULT NULL,
  `meta_k` varchar(255) DEFAULT NULL,
  `description` text,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project`
--

INSERT INTO `project` (`id`, `category_id`, `name`, `meta_t`, `meta_d`, `meta_k`, `description`, `created_at`, `updated_at`) VALUES
(1, 2, 'Мега-крутой коттедж по неизвестному адресу', '', '', '', '<h1>Заголовок</h1><p>Описание</p>', 1551348305, 1553943198);

-- --------------------------------------------------------

--
-- Структура таблицы `project_image`
--

CREATE TABLE `project_image` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `sort` int(11) NOT NULL DEFAULT '100',
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `project_image`
--

INSERT INTO `project_image` (`id`, `project_id`, `image`, `title`, `description`, `sort`, `created_at`) VALUES
(5, 1, '15513488295c77b45d3118a.jpg', NULL, NULL, 100, 1551348831),
(6, 1, '15513488305c77b45e5e258.jpg', NULL, NULL, 100, 1551348831),
(7, 1, '15513488305c77b45ef2e6c.jpg', NULL, NULL, 100, 1551348831),
(8, 1, '15539431045c9f4a40e0e7e.jpg', NULL, NULL, 100, 1553943105),
(9, 1, '15539431135c9f4a492b2fd.jpg', NULL, NULL, 100, 1553943114);

-- --------------------------------------------------------

--
-- Структура таблицы `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `key` varchar(255) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `setting`
--

INSERT INTO `setting` (`id`, `key`, `value`) VALUES
(1, 'category_image_height', '600'),
(2, 'category_image_width', '600'),
(3, 'object_image_height', '600'),
(4, 'object_image_width', '600'),
(5, 'base_url', 'http://dorn'),
(6, 'project_image_height', '600'),
(7, 'project_image_width', '600'),
(8, 'phone_1', '+38 (012) 345-67-89'),
(9, 'phone_2', ''),
(10, 'phone_3', ''),
(12, 'viber', ''),
(13, 'telegram', ''),
(14, 'watsapp', ''),
(15, 'instagram', ''),
(16, 'vk', ''),
(17, 'fb', ''),
(18, 'youtube', ''),
(19, 'email_1', 'email@email.com'),
(20, 'email_2', ''),
(21, 'email_3', ''),
(22, 'widget_slider_image_height', '1600'),
(23, 'widget_slider_image_width', '2560'),
(24, 'footer_text', '<h3>2016 Urban group architects\r\n</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>'),
(25, 'main_meta_t', 'Главная страница'),
(26, 'main_meta_d', ''),
(27, 'main_meta_k', ''),
(28, 'category_substrate_color', '#380000'),
(29, 'category_substrate_opacity', '0.9'),
(30, 'object_substrate_color', '#380000'),
(31, 'object_substrate_opacity', '0.9'),
(32, 'gallery_substrate_color', '#380000'),
(33, 'gallery_substrate_opacity', '0.9'),
(34, 'category_substrate_opacity_hover', '0.7'),
(35, 'object_substrate_opacity_hover', '0.7'),
(36, 'gallery_substrate_opacity_hover', '0.5'),
(37, 'main_color', '#380000'),
(38, 'slider_substrate_color', '#380000'),
(39, 'slider_substrate_opacity', '0.7'),
(45, 'address', 'Одесса, ул. Думская, дом 1');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'admin', '60J9_DxScjzyDokTNDs8CwcJbiBmHoNu', '$2y$13$WFog0tXWx75YTaTAWpOA3uHRCN.kvJ8ZIfcZ6FDVZF27igwpiTWSi', NULL, 'patison.pon.pon@gmail.com', 10, 1551184626, 1551184626);

-- --------------------------------------------------------

--
-- Структура таблицы `widget_slider`
--

CREATE TABLE `widget_slider` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `widget_slider`
--

INSERT INTO `widget_slider` (`id`, `name`, `created_at`) VALUES
(1, 'Слайдер на главной', 1552730806);

-- --------------------------------------------------------

--
-- Структура таблицы `widget_slider_image`
--

CREATE TABLE `widget_slider_image` (
  `id` int(11) NOT NULL,
  `widget_slider_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `sort` int(11) NOT NULL DEFAULT '100',
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `widget_slider_image`
--

INSERT INTO `widget_slider_image` (`id`, `widget_slider_id`, `image`, `title`, `description`, `sort`, `created_at`) VALUES
(11, 1, '15528452215c8e89a5e474b.jpg', 'title 1', NULL, 100, 1552845225),
(12, 1, '15528452375c8e89b5cd16a.jpg', 'title 2', NULL, 100, 1552845241),
(13, 1, '15528452795c8e89df3561f.jpg', 'title 3', NULL, 100, 1552845284),
(14, 1, '15528452935c8e89ed91c4d.jpg', 'title 4', NULL, 100, 1552845298);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `object`
--
ALTER TABLE `object`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `object_image`
--
ALTER TABLE `object_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_id` (`object_id`);

--
-- Индексы таблицы `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `name` (`name`);

--
-- Индексы таблицы `project_image`
--
ALTER TABLE `project_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Индексы таблицы `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Индексы таблицы `widget_slider`
--
ALTER TABLE `widget_slider`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `widget_slider_image`
--
ALTER TABLE `widget_slider_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `object_id` (`widget_slider_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `object`
--
ALTER TABLE `object`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `object_image`
--
ALTER TABLE `object_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `project`
--
ALTER TABLE `project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `project_image`
--
ALTER TABLE `project_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `widget_slider`
--
ALTER TABLE `widget_slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `widget_slider_image`
--
ALTER TABLE `widget_slider_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

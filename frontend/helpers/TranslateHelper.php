<?php

namespace frontend\helpers;

use Yii;

class TranslateHelper
{

    public static function getLocaleCode($field)
    {
        $languages = [
            'ru' => 'ru',
            'en' => 'en',
        ];
        return $field . '_' . $languages[Yii::$app->language];
    }

}
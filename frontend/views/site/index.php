<?php

/* @var $this yii\web\View */

use frontend\helpers\TranslateHelper;

$this->title = Yii::$app->settings->getSetting(TranslateHelper::getLocaleCode('main_meta_t'));
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => Yii::$app->settings->getSetting('main_meta_k'),
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => Yii::$app->settings->getSetting('main_meta_d'),
]);
?>
<h3 class="main-header"><?=Yii::t('common', 1)?></h3>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?=\frontend\widgets\FlexGridWidget::widget([
                'model' => false,
                'items' => $categories,
                'container_class' => 'categories',
                'item_class' => 'category',
                'link' => 'site/project',
                'link_item' => 'id'
            ])?>
        </div>
    </div>
</div>

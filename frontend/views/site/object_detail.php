<?php

/* @var $this yii\web\View */

use frontend\helpers\TranslateHelper;

$this->title = $model->meta_t ? $model->meta_t : $model->{TranslateHelper::getLocaleCode('name')};
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'design'), 'url' => ['/site/projects']];
$this->params['breadcrumbs'][] = ['label' => $model->category->{TranslateHelper::getLocaleCode('name')}, 'url' => ['/site/project', 'id' => $model->category_id]];
$this->params['breadcrumbs'][] = $model->{TranslateHelper::getLocaleCode('name')};
?>
<div class="object">

    <p><?=$model->{TranslateHelper::getLocaleCode('description')}?></p>

    <div class="page-category-container">
        <?=\frontend\widgets\FlexGridWidget::widget([
            'model' => $model,
            'items' => $images,
            'container_class' => 'objects',
            'item_class' => 'object',
            'link' => '',
            'link_item' => 'id',
            'scheme' => 'gallery',
        ])?>
    </div>

</div>

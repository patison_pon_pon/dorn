<?php

/* @var $this yii\web\View */

use frontend\helpers\TranslateHelper;

$this->title = $model->meta_t ? $model->meta_t : $model->{TranslateHelper::getLocaleCode('name')};
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);

$this->params['breadcrumbs'][] = ['label' => Yii::t('common', 'design'), 'url' => ['/site/projects']];
$this->params['breadcrumbs'][] = $model->{TranslateHelper::getLocaleCode('name')};
?>
<div class="project">

    <p><?=$model->{TranslateHelper::getLocaleCode('description')}?></p>

    <div class="page-category-container">
        <?=\frontend\widgets\FlexGridWidget::widget([
            'model' => $model,
            'items' => $projects,
            'container_class' => 'projects',
            'item_class' => 'project',
            'link' => 'site/project-detail',
            'link_item' => 'id',
            'scheme' => 'object',
        ])?>
    </div>

</div>

<?php

/* @var $this yii\web\View */

use frontend\helpers\TranslateHelper;

$this->title = $model->meta_t ? $model->meta_t : $model->{TranslateHelper::getLocaleCode('name')};
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);
$this->params['breadcrumbs'][] = $model->{TranslateHelper::getLocaleCode('name')};
?>
<div class="object">

    <p><?=$model->{TranslateHelper::getLocaleCode('description')}?></p>
    <?=\frontend\widgets\ContactWidget::widget();?>
    <br>

    <iframe height="440" src="https://maps.google.com/maps?width=700&amp;height=440&amp;hl=ru&amp;q=<?=Yii::$app->settings->getSetting(TranslateHelper::getLocaleCode('address'))?>+(dorn)&amp;ie=UTF8&amp;t=&amp;z=18&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</div>

<?php

/* @var $this yii\web\View */

use frontend\helpers\TranslateHelper;

$this->title = $model->meta_t ? $model->meta_t : $model->{TranslateHelper::getLocaleCode('name')};
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);
$this->params['breadcrumbs'][] = $model->{TranslateHelper::getLocaleCode('name')};
?>
<div class="objects">

    <p><?=$model->{TranslateHelper::getLocaleCode('description')}?></p>

    <div class="page-category-container">
        <?=\frontend\widgets\FlexGridWidget::widget([
            'model' => $model,
            'items' => $categories,
            'container_class' => 'categories',
            'item_class' => 'category',
            'link' => 'site/object',
            'link_item' => 'id'
        ])?>
    </div>

</div>

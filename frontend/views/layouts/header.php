<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<header id="page-header">
    <div class="container-fluid">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3">
                        <p class="text-center">
                            <?=Html::a(
                                Html::img('/image/common/logo.jpg'),
                                Yii::$app->homeUrl,
                                ['class' => 'logo']
                            )?>
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-9">
                        <?=\frontend\widgets\MenuWidget::widget([
                            'brand_label' => false,
                            'main_id' => 'main-menu',
                            'nav_id' => 'main-menu-nav',
                        ])?>
                    </div>
                </div>
            </div>
            <?=\frontend\widgets\MenuWidget::widget([
                'brand_label' => Yii::t('common', 'menu'),
                'main_id' => 'mobile-main-menu',
                'nav_id' => 'mobile-main-menu-nav',
            ])?>
        </div>
    </div>
</header>
<div class="container" id="breadcrumb">
    <?= Breadcrumbs::widget(
        [
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]
    ) ?>
</div>

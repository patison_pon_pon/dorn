<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!-- Подключаем хедер -->
<?= $this->render('/layouts/head') ?>
<!-- /Подключаем хедер -->
<?php $this->beginBody() ?>

<!-- Подключаем хедер -->
<?= $this->render('/layouts/header_home') ?>
<!-- /Подключаем хедер -->

<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= Alert::widget() ?>
<?= $content ?>

<?= $this->render('/layouts/footer') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

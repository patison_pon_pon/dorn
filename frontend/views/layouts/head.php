<?php
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
    <style>
        body{
            background: <?=Yii::$app->settings->getSetting('main_color')?>;
        }
        #header:before{
            background: <?=Yii::$app->settings->getSetting('slider_substrate_color')?>;
            opacity: <?=Yii::$app->settings->getSetting('slider_substrate_opacity')?>;
        }

        #lang{
            color: #fff;
            background: <?=Yii::$app->settings->getSetting('main_color')?>;
        }
    </style>
</head>
<body>
<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;

?>
<header id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="container-fluid header-navigation">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <p class="text-center">
                            <?=Html::a(
                                Html::img('/image/common/logo.jpg'),
                                Yii::$app->homeUrl,
                                ['class' => 'logo']
                            )?>
                        </p>
                    </div>
                    <?=\frontend\widgets\MenuWidget::widget([
                        'brand_label' => false,
                        'main_id' => 'main-menu',
                        'nav_id' => 'main-menu-nav',
                    ])?>
                </div>
            </div>
            <?=\frontend\widgets\MainSliderWidget::widget();?>
            <div class="container header-contact">
                <?=\frontend\widgets\ContactWidget::widget();?>
            </div>
            <?=\frontend\widgets\MenuWidget::widget([
                'brand_label' => Yii::t('common', 'menu'),
                'main_id' => 'mobile-main-menu',
                'nav_id' => 'mobile-main-menu-nav',
            ])?>
        </div>
    </div>
</header>

<?php

use frontend\helpers\TranslateHelper;
use frontend\widgets\LanguageSwitcher; ?>
<footer id="footer">
    <div class="container">
        <div class="footer">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <div class="footer-contact">
                        <?=\frontend\widgets\ContactWidget::widget();?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                    <?=Yii::$app->settings->getSetting(TranslateHelper::getLocaleCode('footer_text'))?>
                    <?= LanguageSwitcher::Widget() ?>
                </div>
            </div>
        </div>
    </div>
</footer>
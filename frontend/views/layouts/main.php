<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!-- Подключаем хедер -->
<?= $this->render('/layouts/head') ?>
<!-- /Подключаем хедер -->
<?php $this->beginBody() ?>

<!-- Подключаем хедер -->
<?= $this->render('/layouts/header') ?>
<!-- /Подключаем хедер -->

<?php /*= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])*/ ?>
<?= Alert::widget() ?>
<div class="container">
    <div id="page-wrap">
        <?= $content ?>
    </div>
</div>

<?= $this->render('/layouts/footer') ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

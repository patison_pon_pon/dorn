<?php

namespace frontend\widgets;

use common\models\LoginForm;
use common\models\WidgetSliderImage;
use Yii;
use yii\base\Widget;

class MenuWidget extends Widget {

    public $brand_label;
    public $main_id;
    public $nav_id;


    public function init()
    {
        parent::init();
    }

    public function run() {
        return $this->render('menu', [
            'brand_label' => $this->brand_label,
            'main_id' => $this->main_id,
            'nav_id' => $this->nav_id,
        ]);
    }

}
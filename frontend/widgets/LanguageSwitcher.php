<?php
namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\bootstrap\ButtonDropdown;
use yii\helpers\Url;
use yii\web\Cookie;

class LanguageSwitcher extends Widget
{
    /*public $languages = [
        'ru' => 'Русский',
        'en' => 'English',
    ];

    public function init()
    {
        parent::init();
        var_dump(Yii::$app->language);

        $languageNew = Yii::$app->request->get('language');
        if($languageNew) {
            if(isset($this->languages[$languageNew])) {
                if (Yii::$app->language != $languageNew){
                    Yii::$app->language = $languageNew;
                    setcookie('language', $languageNew, time()+3600*24*30*12, '/');
                    header("Refresh:0");
                }
            }
        }
        elseif(isset($_COOKIE['language'])) {
            Yii::$app->language = $_COOKIE['language'];
        }

    }

    public function run()
    {
        $languages = $this->languages;
        $current = $languages[Yii::$app->language];
        unset($languages[Yii::$app->language]);

        $items = [];
        foreach($languages as $code => $language) {
            $temp = [];
            $temp['label'] = $language;
            $temp['url'] = Url::current(['language' => $code]);
            array_push($items, $temp);
        }

        echo ButtonDropdown::widget([
            'label' => $current,
            'dropdown' => [
                'items' => $items,
            ],
        ]);
    }*/
    public function run() {
        return $this->render('lang');
    }

}
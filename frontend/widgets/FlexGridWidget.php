<?php

namespace frontend\widgets;

use yii\base\Widget;

class FlexGridWidget extends Widget {

    public $model;
    public $items;
    public $container_class;
    public $item_class;
    public $link;
    public $link_item;
    public $scheme;


    public function init()
    {
        parent::init();
        if (!$this->container_class) {
            $this->container_class = '';
        }
        if (!$this->item_class) {
            $this->item_class = '';
        }
        if (!$this->scheme) {
            $this->scheme = 'category';
        }
    }

    public function run() {
        return $this->render('flex_grid', [
            'model' => $this->model,
            'items' => $this->items,
            'container_class' => $this->container_class,
            'item_class' => $this->item_class,
            'link' => $this->link,
            'link_item' => $this->link_item,
            'scheme' => $this->scheme,
        ]);
    }

}
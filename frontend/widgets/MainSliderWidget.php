<?php

namespace frontend\widgets;

use common\models\LoginForm;
use common\models\WidgetSliderImage;
use Yii;
use yii\base\Widget;

class MainSliderWidget extends Widget {

    public function run() {
        $images = WidgetSliderImage::findAll(['widget_slider_id' => 1]);
        return $this->render('main_slider', [
            'images' => $images
        ]);
    }

}
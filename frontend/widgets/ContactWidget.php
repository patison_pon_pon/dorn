<?php

namespace frontend\widgets;

use common\models\LoginForm;
use common\models\Setting;
use common\models\WidgetSliderImage;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class ContactWidget extends Widget {

    public function run() {
        $settings = ArrayHelper::map(Setting::find()->all(), 'key', 'value');
        return $this->render('contact', [
            'settings' => $settings
        ]);
    }

}
<?php

use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

NavBar::begin([
    'brandLabel' => $brand_label,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar',
        'id' => $main_id
    ],
]);
$menuItems = [
    ['label' => Yii::t('common', 'design'), 'url' => ['/site/projects']],
    ['label' => Yii::t('common', 'our_works'), 'url' => ['/site/objects']],
    ['label' => Yii::t('common', 'studio'), 'url' => ['/site/studio']],
];

echo Nav::widget([
    'options' => ['class' => 'navbar-nav ', 'id' => $nav_id],
    'items' => $menuItems,
]);
NavBar::end();
<?php

use frontend\helpers\TranslateHelper;

$count = count($images);
?>
<div id="main-slider-container">
    <div id="main-slider" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php for($i = 0; $i < $count; $i++):?>
                <li data-target="#main-slider" data-slide-to="<?=$i?>" class="<?=($i ? '' : 'active')?>"></li>
            <?php endfor;?>
        </ol>
        <div class="carousel-inner">
            <?php $i = 0; foreach ($images as $image): ?>
                <div class="item <?=($i ? '' : 'active')?>">
                    <?php if ($image->{TranslateHelper::getLocaleCode('title')}): ?>
                        <div class="image-title"><span><?= $image->{TranslateHelper::getLocaleCode('title')} ?></span></div>
                    <?php endif; ?>
                    <img src="<?='/image/widget_slider/thumb_' . $image->image?>">
                </div>
                <?php $i++; endforeach;?>
        </div>
        <!-- Carousel controls -->
        <!--<a class="carousel-control left" href="#main-slider" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>-->
        <a class="carousel-control right main-slider-arrow" href="#main-slider" data-slide="next">
            <?=\yii\helpers\Html::img('/image/assets/slider_arrow.png')?>
        </a>
    </div>
</div>
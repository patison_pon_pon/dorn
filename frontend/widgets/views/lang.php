<?php

use yii\helpers\Url;

$ru = Url::current(['lang' => 'ru']);
$en = Url::current(['lang' => 'en']);
$script = <<< JS
$(document).ready(function () {
    $('#lang').on('change', function () {
        switch ($(this).val()) {
            case 'ru':
                location.href = '$ru';
                break;
            case 'en':
                location.href = '$en';
                break;
        }
    })
})


JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>
<select class="select footer_lang_select" id="lang">
    <option value="ru" <?=Yii::$app->language == 'ru' ? 'selected' : ''?>>Русский</option>
    <option value="en" <?=Yii::$app->language == 'en' ? 'selected' : ''?>>English</option>
</select>
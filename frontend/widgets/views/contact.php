<?php

use frontend\helpers\TranslateHelper;
use rmrevin\yii\fontawesome\FA;
?>
<div class="contact-widget">
    <?php
    foreach ($settings as $key => $value) {
        if ($value){
            switch ($key){
                case 'phone_1':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('phone') . '<span>' . $value . '</span>';
                    break;
                case 'phone_2':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('phone') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'phone_3':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('phone') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case TranslateHelper::getLocaleCode('address'):
                    echo "<p class='contact-widget-item'>";
                    echo '<b>' . FA::icon('map-marker') . '<span>' . $value . '</span>' . '</b>';
                    echo '</p>';
                    break;
                case 'viber':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('whatsapp') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'telegram':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('telegram') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'watsapp':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('whatsapp') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'fb':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('facebook-square') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'vk':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('vk') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'instagram':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('instagram') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'youtube':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('youtube') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'email_1':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('envelope') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'email_2':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('envelope') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
                case 'email_3':
                    echo "<p class='contact-widget-item'>";
                    echo FA::icon('envelope') . '<span>' . $value . '</span>';
                    echo '</p>';
                    break;
            }
        }
    }?>
</div>
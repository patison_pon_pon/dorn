<?php

use frontend\helpers\TranslateHelper;
use yii\helpers\Html;
use yii\helpers\Url;

if ($scheme == 'gallery'){
    $options = [
        'class' => 'flex-item-link',
        'data-lightbox' => 'big-image',
        'data-title' => '',
        'data-desc' => '',
    ];
}else{
    $options = [
        'class' => 'flex-item-link',
    ];
}

?>
<style>
    .flex-grid .flex-grid-item:before{
        background: <?=Yii::$app->settings->getSetting($scheme . '_substrate_color')?>;
        opacity: <?=Yii::$app->settings->getSetting($scheme . '_substrate_opacity')?>;
    }
    .flex-grid .flex-grid-item:hover:before{
        opacity: <?=Yii::$app->settings->getSetting($scheme . '_substrate_opacity_hover')?>;
    }
    .flex-grid{
        background: <?=Yii::$app->settings->getSetting('main_color')?>;
    }
</style>
<?php if (!$model || !$model->{TranslateHelper::getLocaleCode('description')}): ?>
    <style>
        #page-wrap, .page-category-container{
            background: <?=Yii::$app->settings->getSetting('main_color')?>;
            padding-top: 0;
        }
    </style>
<?php endif; ?>
<div class="flex-grid <?=$container_class?> <?=$scheme == 'object' || $scheme == 'gallery' ? 'flex-grid-objects' : ''?>">
    <?php foreach ($items as $item):?>
        <div class="flex-grid-item  <?=$item_class?>">
            <?php
            echo Html::img('/image/' . $item_class .'/thumb_' . (
                    is_object($item->image)
                    ? $item->image->image
                    : $item->image
                ), ['class' => 'flex-grid-image']);
            // if (isset($item->icon)){
            //     echo Html::img('/image/' . $item_class .'/thumb_' . $item->icon, ['class' => 'flex-grid-icon']);
            // }
            ?>

            <?php if (isset($item->{TranslateHelper::getLocaleCode('name')})): ?>
            <div class="circle"><span></span></div>
            <h4 class="flex-grid-name"><span><?= $item->{TranslateHelper::getLocaleCode('name')} ?></span></h4>
            <?php endif; ?>

            <?=Html::a('', (
                $scheme == 'gallery'
                    ?  '/image/' . $item_class .'/' . $item->image
                    : Url::to([$link, $link_item => $item->$link_item])
            ), $options)?>
        </div>
    <?php endforeach; ?>
</div>
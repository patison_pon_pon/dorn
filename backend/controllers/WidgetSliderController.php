<?php

namespace backend\controllers;

use backend\models\ImageForm;
use common\models\WidgetSliderImage;
use Yii;
use common\models\WidgetSlider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WidgetSliderController implements the CRUD actions for WidgetSlider model.
 */
class WidgetSliderController extends Controller
{
    public $scheme = 'widget_slider';
    public $image_model = WidgetSliderImage::class;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = new ImageForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($image->load(Yii::$app->request->post())) {

            if ($_FILES){
                Yii::$app->imageHelper->saveImageMultiple($image, $this->scheme, $this->image_model, $model->id);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'image' => $image,
        ]);
    }

    public function actionImageList($model_id)
    {
        $image_list = $this->image_model::findAll([$this->scheme . '_id' => $model_id]);

        return $this->renderPartial('image_list', [
            'image_list' => $image_list,
            'scheme' => $this->scheme,
        ]);
    }

    public function actionDeleteImage($id)
    {
        $image = $this->image_model::findOne($id);
        if ($image){
            Yii::$app->imageHelper->deleteImage($image->image, $this->scheme);
            $image->delete();
        }
    }

    public function actionSaveImageData()
    {

        if (Yii::$app->request->isAjax) {
            $data = Yii::$app->request->get();
            $image = $this->image_model::findOne($data['id']);
            $image->load($data, '');
            $image->save();
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = WidgetSlider::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

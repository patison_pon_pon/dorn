<?php

namespace backend\controllers;

use backend\models\ImageForm;
use common\models\ProjectImage;
use Yii;
use common\models\Project;
use common\models\ProjectSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends Controller
{
    public $scheme = 'project';
    public $image_model = ProjectImage::class;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Project();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $image = new ImageForm();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        if ($image->load(Yii::$app->request->post())) {

            if ($_FILES){
                Yii::$app->imageHelper->saveImageMultiple($image, $this->scheme, $this->image_model, $model->id);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'image' => $image,
        ]);
    }

    public function actionImageList($model_id)
    {
        $image_list = $this->image_model::find()->where([$this->scheme . '_id' => $model_id])->orderBy('sort')->all();

        return $this->renderPartial('image_list', [
            'image_list' => $image_list,
            'scheme' => $this->scheme,
        ]);
    }

    public function actionDeleteImage($id)
    {
        $image = $this->image_model::findOne($id);
        if ($image){
            Yii::$app->imageHelper->deleteImage($image->image, $this->scheme);
            $image->delete();
        }
    }

    public function actionSortImages()
    {
        $data = Yii::$app->request->post();
        foreach ($data['images'] as $key => $value) {
            $this->image_model::updateAll(['sort' => $key], ['id' => $value]);
        }
        return print_r($data);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

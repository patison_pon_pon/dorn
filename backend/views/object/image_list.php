<?php

use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
?>
    <style>
        .image{
            position: relative;
            margin: 15px;
            display: inline-block;
        }
        .image .delete-image{
            position: absolute;
            right: -13px;
            top: -13px;
            opacity: 0;
            transition: all .15s linear;
        }
        .image:hover .delete-image{
            opacity: .7;
        }
        .image:hover .delete-image:hover{
            opacity: 1;
        }
    </style>
<?php foreach ($image_list as $item):?>
    <div class="image">
        <?=Html::a(
            Html::img(
                Yii::$app->dataHelper->imageWebPath($scheme) . 'thumb_' . $item->image,
                [
                    'class' => 'img-thumbnail scheme-image',
                    'width' => 280
                ]
            ),
            Yii::$app->dataHelper->imageWebPath($scheme) . $item->image,
            [
                'data-lightbox' => 'big-image',
                'data-title' => '',
                'data-desc' => '',
            ]
        )?>
        <?=Html::button(FA::icon('remove'), [
            'class' => 'delete-image btn btn-danger',
            'data-id' => $item->id,
            'title' => 'Удалить',
            /*'data' => [
                'confirm' => 'Точно удалить?',
            ],*/
        ])?>
    </div>
<?php endforeach; ?>
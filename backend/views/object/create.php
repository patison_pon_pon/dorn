<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Object */

$this->title = 'Создать обьект';
$this->params['breadcrumbs'][] = ['label' => 'Обьекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="object-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <p class="text-right">
        <?= Html::a('Создать проект', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_ru',
            [
                'attribute' => 'category_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->category ? $model->category->name_ru : null;
                },
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Category::find()->all(), 'id', 'name_ru'),
            ],
            //'meta_k',
            //'description:ntext',
            //'created_at',
            //'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия&nbsp;&nbsp;&nbsp;',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'View',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Точно удалить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>

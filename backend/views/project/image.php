<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


Pjax::begin();

$get_image_list = Url::to(["project/image-list"]);
$delete_image = Url::to(["project/delete-image"]);
$sort_images = Url::to(["project/sort-images"]);
$param = Yii::$app->getRequest()->csrfParam;
$token = Yii::$app->getRequest()->getCsrfToken();
$id = $model->id;

$script = <<< JS

    function getImageList() {        
        $.ajax({
            url: '$get_image_list',
            cache: false,
            type: "GET",
            data: { 'model_id': $id},
            success: function (data) {
                $('.image-list').html(data);
            }
        });
    }
    
    getImageList();

    $('.image-list').on('click', '.delete-image', function() { 
        $.ajax({
            url: '$delete_image',
            cache: false,
            type: "GET",
            data: { 'id': $(this).data('id')},
            success: function (data) {
                getImageList();
            }
        });
    });
    
    $(".image-list").sortable({
        items:".image",
        update: function (event, ui) {
            var data = {
                'images': $(this).sortable('toArray'), 
                "{$param}": '$token', 
            };
            console.log(data);
            $.ajax({
                url: '$sort_images',
                type: 'POST',
                data: data,
                success: function (data) {
                    console.log(data);
                }
            });
        }
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

$form = ActiveForm::begin(['options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data']]);
?>

<?= $form->field($image, 'image[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

<div class="image-list">

</div>

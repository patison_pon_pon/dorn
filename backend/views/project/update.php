<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Project */

$this->title = 'Редактировать проект: ' . $model->name_ru;
$this->params['breadcrumbs'][] = ['label' => 'Проекты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name_ru;
?>
<div class="project-update">

    <!-- TAB NAVIGATION -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#tab1" role="tab" data-toggle="tab">Основное</a></li>
        <li><a href="#tab2" role="tab" data-toggle="tab">Фото</a></li>
    </ul>
    <!-- TAB CONTENT -->
    <div class="tab-content">
        <div class="active tab-pane fade in" id="tab1">
            <br>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
        <div class="tab-pane fade" id="tab2">
            <br>
            <?php include 'image.php'?>
        </div>
    </div>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <p class="text-right">
        <?= Html::a('Создать категорию', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id',
            'name_ru',
            //'meta_t',
            //'meta_d',
            //'meta_k',
            //'description:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(
                        Html::img(
                            Yii::$app->dataHelper->imageWebPath('category') . 'thumb_' . $model->image,
                            [
                                'class' => 'img-thumbnail scheme-image',
                                'width' => 120
                            ]
                        ),
                        Yii::$app->dataHelper->imageWebPath('category') . $model->image,
                        [
                            'data-lightbox' => 'big-image',
                            'data-title' => '',
                            'data-desc' => '',
                        ]
                    );
                },
                'filter' => '',
                'enableSorting' => false
            ],
            //'created_at',
            //'updated_at',


            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия&nbsp;&nbsp;&nbsp;',
                'headerOptions' => ['width' => '90'],
                'template' => '{update}&nbsp;{delete}',
                'buttons' => [
                    'update' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-sm',
                            'title' => 'Редактировать',
                        ]);
                    },
                    'view' => function($url, $model){
                        return Html::a('<span class="fa fa-eye"></span>', ['view', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'title' => 'View',
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить',
                            'data' => [
                                'confirm' => 'Точно удалить?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>
</div>

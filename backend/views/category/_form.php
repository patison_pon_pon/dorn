<?php

use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_ru')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'name_en')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_ru')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
        ],
    ]);
    ?>

    <?= $form->field($model, 'description_en')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
            ],
        ],
    ]);
    ?>

    <?php if ($model->image):?>
        <?=Html::a(
            Html::img(
                Yii::$app->dataHelper->imageWebPath('category') . 'thumb_' . $model->image,
                [
                    'class' => 'img-thumbnail scheme-image',
                    'width' => 280
                ]
            ),
            Yii::$app->dataHelper->imageWebPath('category') . $model->image,
            [
                'data-lightbox' => 'big-image',
                'data-title' => '',
                'data-desc' => '',
            ]
        );?>
    <?php endif;?>

    <?= $form->field($model, 'image')->fileInput() ?>


    <?php /*if ($model->icon):?>
        <?=Html::a(
            Html::img(
                Yii::$app->dataHelper->imageWebPath('category') . 'thumb_' . $model->icon,
                [
                    'class' => 'img-thumbnail scheme-image',
                    'width' => 280
                ]
            ),
            Yii::$app->dataHelper->imageWebPath('category') . $model->icon,
            [
                'data-lightbox' => 'big-image',
                'data-title' => '',
                'data-desc' => '',
            ]
        );?>
    <?php endif;*/?>

    <!-- <?= $form->field($model, 'icon')->fileInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

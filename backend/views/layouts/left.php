<aside class="main-sidebar">

    <section class="sidebar">

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Главное меню', 'options' => ['class' => 'header']],
                    ['label' => 'Категории', 'icon' => 'circle-o', 'url' => ['/category/index']],
                    ['label' => 'Проекты', 'icon' => 'circle-o', 'url' => ['/project/index']],
                    ['label' => 'Обьекты', 'icon' => 'circle-o', 'url' => ['/object/index']],
                    ['label' => 'Страницы', 'icon' => 'circle-o', 'url' => ['/page/index']],
                    ['label' => 'Настройки', 'icon' => 'circle-o', 'url' => ['/setting/common']],
                    ['label' => 'Слайдер на главной', 'icon' => 'circle-o', 'url' => ['widget-slider/update', 'id' => 1]],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>

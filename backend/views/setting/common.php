<?php

use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model backend\models\settings\Settings */

$this->title = 'Настройки';
//$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Настройки';
?>
<div class="settings-update">

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

            <div class="form-group">
                <?=Html::label('Телефон', '', ['class' => 'control-label'])?>
                <?=MaskedInput::widget([
                    'name' => "settings[phone_1]",
                    'mask' => '+38 (999) 999-99-99',
                    'value' => $settings['phone_1']
                ]);?>
            </div>

            <div class="form-group">
                <?=Html::label('Адрес RU', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[address_ru]", $settings['address_ru'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Адрес EN', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[address_en]", $settings['address_en'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('E-mail', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[email_1]", $settings['email_1'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Meta title главной страницы RU', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[main_meta_t_ru]", $settings['main_meta_t_ru'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Meta title главной страницы EN', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[main_meta_t_en]", $settings['main_meta_t_en'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Цвет подложки для категорий', '', ['class' => 'control-label'])?>
                <?=Html::input('color', "settings[category_substrate_color]", $settings['category_substrate_color'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки для категорий', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[category_substrate_opacity]", $settings['category_substrate_opacity'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки для категорий при наведении', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[category_substrate_opacity_hover]", $settings['category_substrate_opacity_hover'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Цвет подложки для обьекта', '', ['class' => 'control-label'])?>
                <?=Html::input('color', "settings[object_substrate_color]", $settings['object_substrate_color'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки для обьекта', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[object_substrate_opacity]", $settings['object_substrate_opacity'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки для обьекта при наведении', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[object_substrate_opacity_hover]", $settings['object_substrate_opacity_hover'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Цвет подложки для галереи', '', ['class' => 'control-label'])?>
                <?=Html::input('color', "settings[gallery_substrate_color]", $settings['gallery_substrate_color'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки для галереи', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[gallery_substrate_opacity]", $settings['gallery_substrate_opacity'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки для галереи при наведении', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[gallery_substrate_opacity_hover]", $settings['gallery_substrate_opacity_hover'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

            <div class="form-group">
                <?=Html::label('Высота изображения категории (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[category_image_height]", $settings['category_image_height'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Ширина изображения категории (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[category_image_width]", $settings['category_image_width'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Высота изображения обьекта (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[object_image_height]", $settings['object_image_height'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Ширина изображения обьекта (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[object_image_width]", $settings['object_image_width'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Высота изображения проекта (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[project_image_height]", $settings['project_image_height'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Ширина изображения проекта (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[project_image_width]", $settings['project_image_width'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Высота изображения слайдера на главной (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[widget_slider_image_height]", $settings['widget_slider_image_height'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Ширина изображения слайдера на главной (px)', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[widget_slider_image_width]", $settings['widget_slider_image_width'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Основной адрес', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[base_url]", $settings['base_url'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Instagram', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[instagram]", $settings['instagram'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('vk', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[vk]", $settings['vk'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Facebook', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[fb]", $settings['fb'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Youtube', '', ['class' => 'control-label'])?>
                <?=Html::input('text', "settings[youtube]", $settings['youtube'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Основной цвет сайта', '', ['class' => 'control-label'])?>
                <?=Html::input('color', "settings[main_color]", $settings['main_color'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Цвет подложки слайдера', '', ['class' => 'control-label'])?>
                <?=Html::input('color', "settings[slider_substrate_color]", $settings['slider_substrate_color'], ['class' => 'form-control'] )?>
            </div>

            <div class="form-group">
                <?=Html::label('Прозрачность подложки слайдера', '', ['class' => 'control-label'])?>
                <?=Html::input('number', "settings[slider_substrate_opacity]", $settings['slider_substrate_opacity'], ['class' => 'form-control', 'min' => '0', 'max' => '1.0', 'step' => '0.1'] )?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <?=Html::label('Теест в футере RU', '', ['class' => 'control-label'])?>
                <?php
                echo Widget::widget([
                    'value' => $settings['footer_text_ru'],
                    'name' => "settings[footer_text_ru]",
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'maxHeight' => 600,
                        'plugins' => [
                            'clips',
                            'fullscreen'
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <?=Html::label('Теест в футере EN', '', ['class' => 'control-label'])?>
                <?php
                echo Widget::widget([
                    'value' => $settings['footer_text_en'],
                    'name' => "settings[footer_text_en]",
                    'settings' => [
                        'lang' => 'ru',
                        'minHeight' => 200,
                        'maxHeight' => 600,
                        'plugins' => [
                            'clips',
                            'fullscreen'
                        ]
                    ]
                ]);
                ?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>



    <?php ActiveForm::end(); ?>

</div>

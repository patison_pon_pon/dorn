<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


Pjax::begin();

$get_image_list = Url::to(["widget-slider/image-list"]);
$delete_image = Url::to(["widget-slider/delete-image"]);
$id = $model->id;
$save_image_name = Url::to(["widget-slider/save-image-data"]);

$script = <<< JS

    function getImageList() {        
        $.ajax({
            url: '$get_image_list',
            cache: false,
            type: "GET",
            data: { 'model_id': $id},
            success: function (data) {
                $('.image-list').html(data);
            }
        });
    }
    
    getImageList();

    $('.image-list').on('click', '.delete-image', function() { 
        $.ajax({
            url: '$delete_image',
            cache: false,
            type: "GET",
            data: { 'id': $(this).data('id')},
            success: function (data) {
                getImageList();
            }
        });
    });

    $('.image-list').on('keyup', "input[name='image-title']", function() { 
        $.ajax({
            url: '$save_image_name',
            cache: false,
            type: "GET",
            data: { 'id': $(this).data('id'), 'title': $(this).val()},
            success: function (data) {
                //getImageList();
            }
        });
    });
JS;
$this->registerJs($script, yii\web\View::POS_READY);

$form = ActiveForm::begin(['options' => ['data-pjax' => true, 'enctype' => 'multipart/form-data']]);
?>

<?= $form->field($image, 'image[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>

<div class="image-list">

</div>

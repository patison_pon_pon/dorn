
$(document).ajaxStart(function () {
    $('#loader').show();
    console.log('start');
});
$(document).ajaxComplete(function () {
    setTimeout(function() {
        $('#loader').fadeOut('slow');
    }, 1000);
    console.log('end');
});
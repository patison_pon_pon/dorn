<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\helpers\Url;

class DataHelper extends Component
{

    public function baseUrl()
    {
        return 'http://restarapp.maximustest.ru';
    }

    public function imagePath($scheme)
    {
        return Yii::getAlias('@frontend') . '/web/image/' . $scheme . '/';
    }

    public function imageWebPath($scheme)
    {
        return Yii::$app->settings->getSetting('base_url') . '/image/' . $scheme . '/';
    }
}
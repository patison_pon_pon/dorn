<?php

namespace common\components;

use common\models\Setting;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class Settings extends Component
{
    public $settings;
    public $language;

    /**
     * Создаем в компоненте массив с настройками
     * @return array
     */
    public function initSettings()
    {
        return $this->settings = ArrayHelper::map(Setting::find()->asArray()->all(), 'key', 'value');
    }

    /**
     * получаем одну настройку, если массив пустой, дергаем создание массива
     * @param $key
     * @return mixed
     */
    public function getSetting($key)
    {
        if (!$this->settings)
            $this->initSettings();

        return $this->settings[$key];
    }

    /**
     * получаем все настройки, если массив пустой, дергаем создание массива
     * @return mixed
     */
    public function getSettings()
    {
        if (!$this->settings)
            $this->initSettings();

        return $this->settings;
    }

    /**
     * записать данные в сессию, требует ключ и значение
     * @param $key
     * @param $value
     * @return bool
     */
    public function addToSession($key, $value)
    {
        $session = Yii::$app->session;
        $session->open();
        $session->set($key, $value);
        $session->close();
        return true;
    }
}
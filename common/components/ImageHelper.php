<?php

namespace common\components;

use Gumlet\ImageResize;
use Yii;
use yii\base\Component;
use yii\web\UploadedFile;

class ImageHelper extends Component
{

    public function deleteImage($image, $scheme){

        if ($image){
            if (file_exists(Yii::$app->dataHelper->imagePath($scheme) . $image)){
                unlink(Yii::$app->dataHelper->imagePath($scheme) . $image);
            }

            if (file_exists(Yii::$app->dataHelper->imagePath($scheme) . 'thumb_' . $image)){
                unlink(Yii::$app->dataHelper->imagePath($scheme) . 'thumb_' . $image);
            }
        }
    }

    public function saveImage($model, $scheme, $crop = 1, $key = 'image'){

        $file = UploadedFile::getInstance($model, $key);
        $result = $this->saveFile($file, $scheme, $crop);

        if ($result){
            if (isset($model->oldAttributes[$key])){
                $this->deleteImage($model->oldAttributes[$key], $scheme);
            }
            $model->$key = $result;
        }else{
            $model->$key = $model->oldAttributes[$key];
        }

        return $model;
    }

    public function saveImageMultiple($model, $scheme, $class, $id, $crop = 1)
    {
        $model->image = UploadedFile::getInstances($model, 'image');
        $result = false;
        foreach ($model->image as $file) {
            $result[] = $this->saveFile($file, $scheme, $crop);
        }
        if ($result){
            $scheme_id = $scheme . '_id';
            foreach ($result as $file) {
                $new_image = new $class;
                $new_image->$scheme_id = $id;
                $new_image->image = $file;
                $new_image->created_at = time();
                if (!$new_image->save()){
                    //$new_image->save();
                    return var_dump($new_image->errors);
                }
            }
        }
        return $result;
    }

    public function saveImageByName($name, $scheme, $crop = 1){

        $file = UploadedFile::getInstanceByName($name);
        var_dump($file);
        $result = $this->saveFile($file, $scheme, $crop);

        return $result;
    }

    public function saveFile($file, $scheme, $crop = 1){

        if ($file){
            $filename = time() . uniqid() . '.' . $file->getExtension();

            //сохранили оригинал
            $file->saveAs(
                Yii::$app->dataHelper->imagePath($scheme)
                . $filename
            );

            if ($crop){
                //сохранили миниатюру
                $img = Yii::$app->dataHelper->imagePath($scheme)
                    . $filename
                ;
                $sizes = $this->sizeName($scheme);
                $width = (int)Yii::$app->settings->getSetting($sizes['0']);
                $height = (int)Yii::$app->settings->getSetting($sizes['1']);

                $thumb = new ImageResize($img);

                $thumb->crop($width, $height, true, ImageResize::CROPCENTER);

                $thumb->save(
                    Yii::$app->dataHelper->imagePath($scheme)
                    . '/thumb_'
                    . $filename
                );
            }

            return $filename;
        }

        return false;
    }

    public function sizeName($scheme)
    {
        return [
            $scheme . '_image_width',
            $scheme . '_image_height',
        ];
    }
}
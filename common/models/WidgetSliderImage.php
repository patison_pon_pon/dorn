<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widget_slider_image".
 *
 * @property int $id
 * @property int $widget_slider_id
 * @property string $image
 * @property string $title_ru
 * @property string $title_en
 * @property string $description_ru
 * @property string $description_en
 * @property int $sort
 * @property int $created_at
 */
class WidgetSliderImage extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'widget_slider_image';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['widget_slider_id', 'image'], 'required'],
            [['widget_slider_id', 'sort', 'created_at'], 'integer'],
            [['description_ru', 'description_en', ], 'string'],
            [['image', 'title_ru', 'title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'widget_slider_id' => 'Widget Slider ID',
            'image' => 'Image',
            'title_ru' => 'Title RU',
            'title_en' => 'Title EN',
            'description_ru' => 'Описание RU',
            'description_en' => 'Описание EN',
            'sort' => 'Sort',
            'created_at' => 'Created At',
        ];
    }
}
